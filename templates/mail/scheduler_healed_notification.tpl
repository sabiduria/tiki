<b>{tr}Notice{/tr}</b>

{tr _0=$schedulerName _1=$stalledTimeout}Scheduler "%0" is now marked as healed, since its running time is over %1 minutes.{/tr}
<br>
<b>{tr}Details{/tr}</b>
{tr}Site:{/tr} {$siteName}
